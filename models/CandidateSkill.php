<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "candidate_skill".
 *
 * @property int $id
 * @property string $name Наименование
 *
 * @property CandidateCandidateSkill[] $candidateCandidateSkills
 */
class CandidateSkill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate_skill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateCandidateSkills()
    {
        return $this->hasMany(CandidateCandidateSkill::className(), ['candidate_skill_id' => 'id']);
    }
}
