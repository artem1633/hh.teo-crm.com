<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;

/**
 * ProjectSearch represents the model behind the search form about `app\models\Project`.
 */
class ProjectSearch extends Project
{
    public $showHidden;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'type_id', 'status_id', 'accountable_user_id', 'created_by', 'archive', 'showHidden'], 'integer'],
            [['title', 'date_start', 'date_end', 'comment', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'type_id' => $this->type_id,
            'status_id' => $this->status_id,
            'accountable_user_id' => $this->accountable_user_id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if(Yii::$app->user->identity->role == User::ROLE_HR) {
            $query->andWhere(['accountable_user_id' => Yii::$app->user->getId()]);
        }

        if($this->showHidden == 1){
            $query->andWhere(['archive' => 1]);
        } else {
            $query->andWhere(['archive' => 0]);
        }

        return $dataProvider;
    }
}
