<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * TaskSearch represents the model behind the search form about `app\models\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'executor_id', 'candidate_id', 'remember_time', 'status_id', 'type_id', 'created_by'], 'integer'],
            [['executed_at', 'text', 'result_text', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('project');
        $query->joinWith('executor');
        $query->joinWith('candidate');

        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'executor_id' => $this->executor_id,
            'candidate_id' => $this->candidate_id,
            'executed_at' => $this->executed_at,
            'remember_time' => $this->remember_time,
            'status_id' => $this->status_id,
            'type_id' => $this->type_id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'result_text', $this->result_text]);

        return $dataProvider;
    }
}
