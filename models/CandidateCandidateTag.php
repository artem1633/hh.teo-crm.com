<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "candidate_candidate_tag".
 *
 * @property int $id
 * @property int $candidate_id
 * @property int $candidate_tag_id
 *
 * @property Candidate $candidate
 * @property CandidateTag $candidateTag
 */
class CandidateCandidateTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate_candidate_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['candidate_id', 'candidate_tag_id'], 'integer'],
            [['candidate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Candidate::className(), 'targetAttribute' => ['candidate_id' => 'id']],
            [['candidate_tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => CandidateTag::className(), 'targetAttribute' => ['candidate_tag_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'candidate_id' => 'Candidate ID',
            'candidate_tag_id' => 'Candidate Tag ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(Candidate::className(), ['id' => 'candidate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateTag()
    {
        return $this->hasOne(CandidateTag::className(), ['id' => 'candidate_tag_id']);
    }
}
