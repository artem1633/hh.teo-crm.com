<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_step".
 *
 * @property int $id
 * @property int $project_id Проект
 * @property string $name Наименование
 * @property integer $sorting
 * @property string $color Цвет
 *
 * @property CandidateProjectStep[] $candidateProjectSteps
 * @property Project $project
 */
class ProjectStep extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_step';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'sorting'], 'integer'],
            [['name', 'color'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Проект',
            'name' => 'Наименование',
            'sorting' => 'Сортировка',
            'color' => 'Цвет',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateProjectSteps()
    {
        return $this->hasMany(CandidateProjectStep::className(), ['step_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return string
     */
    public function getRealColor()
    {
        return $this->color != null ? $this->color : '#cecece';
    }
}
