<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $project_id Проект
 * @property int $executor_id Исполнитель
 * @property int $candidate_id Кандидат
 * @property string $executed_at Дата и время выполнения
 * @property int $remember_time Время напоминания (в мин)
 * @property int $status_id Статус
 * @property int $type_id Тип
 * @property string $text Текст
 * @property string $result_text Результат
 * @property int $created_by
 * @property string $created_at
 *
 * @property Candidate $candidate
 * @property User $createdBy
 * @property User $executor
 * @property Project $project
 * @property TaskStatus $status
 * @property TaskType $type
 */
class Task extends \yii\db\ActiveRecord
{
    public $files;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => null,
                'createdByAttribute' => 'created_by',
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'executor_id', 'text'], 'required'],
            [['project_id', 'executor_id', 'candidate_id', 'remember_time', 'status_id', 'type_id', 'created_by'], 'integer'],
            [['executed_at', 'created_at', 'files'], 'safe'],
            [['text', 'result_text'], 'string'],
            [['candidate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Candidate::className(), 'targetAttribute' => ['candidate_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['executor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['executor_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Вакансия',
            'executor_id' => 'Исполнитель',
            'candidate_id' => 'Кандидат',
            'executed_at' => 'Дата и время выполнения',
            'remember_time' => 'Время напоминания (в мин)',
            'status_id' => 'Статус',
            'type_id' => 'Тип',
            'text' => 'Текст',
            'result_text' => 'Результат',
            'created_by' => 'Дата и время создания',
            'created_at' => 'Кем создан',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $files = explode(',', $this->files);

        foreach ($files as $file){
            if($file != null){
                $file = TaskFile::findOne($file);
                $file->task_id = $this->id;
                $file->save(false);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(Candidate::className(), ['id' => 'candidate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor()
    {
        return $this->hasOne(User::className(), ['id' => 'executor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(TaskStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'type_id']);
    }
}
