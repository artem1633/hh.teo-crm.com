<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "candidate_tag".
 *
 * @property int $id
 * @property string $name Наименование
 *
 * @property CandidateCandidateTag[] $candidateCandidateTags
 */
class CandidateTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateCandidateTags()
    {
        return $this->hasMany(CandidateCandidateTag::className(), ['candidate_tag_id' => 'id']);
    }
}
