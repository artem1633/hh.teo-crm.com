<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\CandidateTag;

/**
 * This is the model class for table "candidate".
 *
 * @property int $id
 * @property string $name ФИО
 * @property string $email Email
 * @property string $phone Телефон
 * @property string $city Город
 * @property string $comment Комментарий
 * @property string $education Образование
 * @property string $work_experience Опыт работы
 * @property string $age Возраст
 * @property string $languages Знание языков
 * @property string $additional Дополнительная информация
 * @property string $doc_file Документ
 * @property string $created_at
 *
 * @property CandidateCandidateTag[] $candidateCandidateTags
 * @property CandidateContact[] $candidateContacts
 * @property CandidateProjectStage[] $candidateProjectStages
 * @property Task[] $tasks
 */
class Candidate extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $tags;

    /**
     * @var array
     */
    public $skills;

    /**
     * @var
     */
    public $files;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['comment', 'education', 'work_experience', 'languages', 'additional', 'doc_file'], 'string'],
            [['created_at', 'tags', 'skills', 'files'], 'safe'],
            ['email', 'email'],
            [['name', 'email', 'phone', 'city', 'age'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'city' => 'Город',
            'comment' => 'Комментарий',
            'education' => 'Образование',
            'work_experience' => 'Опыт работы',
            'age' => 'Возраст',
            'languages' => 'Знание языков',
            'additional' => 'Дополнительная информация',
            'doc_file' => 'Документ',
            'tags' => 'Тэги',
            'skills' => 'Навыки',
            'created_at' => 'Дата и время создания',
        ];
    }



    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        CandidateCandidateTag::deleteAll(['candidate_id' => $this->id]);
        if($this->tags != null){
            foreach ($this->tags as $tag) {
                $candidateTag = CandidateTag::find()->where(['name' => $tag])->one();
                if($candidateTag == null){
                    $candidateTag = new CandidateTag(['name' => $tag]);
                    $candidateTag->save(false);
                }
                (new CandidateCandidateTag([
                    'candidate_id' => $this->id,
                    'candidate_tag_id' => $candidateTag->id,
                ]))->save(false);
            }
        }

        CandidateCandidateSkill::deleteAll(['candidate_id' => $this->id]);
        if($this->skills != null){
            foreach ($this->skills as $skill) {
                $candidateSkill = CandidateSkill::find()->where(['name' => $skill])->one();
                if($candidateSkill == null){
                    $candidateSkill = new CandidateSkill(['name' => $skill]);
                    $candidateSkill->save(false);
                }
                (new CandidateCandidateSkill([
                    'candidate_id' => $this->id,
                    'candidate_skill_id' => $candidateSkill->id,
                ]))->save(false);
            }
        }

        $files = explode(',', $this->files);

        foreach ($files as $file){
            if($file != null){
                $file = CandidateFile::findOne($file);
                $file->candidate_id = $this->id;
                $file->save(false);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateCandidateTags()
    {
        return $this->hasMany(CandidateCandidateTag::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateContacts()
    {
        return $this->hasMany(CandidateContact::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateProjectStages()
    {
        return $this->hasMany(CandidateProjectStage::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['candidate_id' => 'id']);
    }
}
