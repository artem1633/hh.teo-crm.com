<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login Логин
 * @property string $name ФИО
 * @property int $position_id Должность
 * @property string $phone Телефон
 * @property int $role Роль
 * @property string $avatar Аватар
 * @property string $password_hash Зашифрованный пароль
 * @property int $access Доступ
 * @property int $is_deletable Можно удалить или нельзя
 * @property string $created_at
 *
 * @property Project[] $projects
 * @property Project[] $projects0
 * @property Task[] $tasks
 * @property Task[] $tasks0
 * @property Ticket[] $tickets
 * @property Position $position
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const ROLE_ADMIN = 0;
    const ROLE_MANAGER = 1;
    const ROLE_HR = 2;

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    public $password;

    private $oldPasswordHash;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name', 'login', 'is_deletable', 'password', 'password_hash', 'position_id', 'phone'],
            self::SCENARIO_EDIT => ['name', 'login', 'is_deletable', 'password', 'password_hash', 'role', 'position_id', 'phone'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
            ['login', 'email'],
//            ['login', 'match', 'pattern' => '/^[a-z]+([-_]?[a-z0-9]+){0,2}$/i', 'message' => '{attribute} должен состоять только из латинских букв и цифр'],
//            ['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,50}$/', 'message' => '{attribute} не соответствует всем параметрам безопасности'],
            [['login'], 'unique'],
            [['position_id', 'role', 'access', 'is_deletable'], 'integer'],
            [['login', 'password_hash', 'password', 'name', 'avatar'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if($uid == $this->id)
        {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if($this->is_deletable == false)
        {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return string
     */
    public function getRealAvatarPath()
    {
        return $this->avatar != null ? $this->avatar : 'img/nouser.png';
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->password != null){
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            Yii::$app->mailer->compose()
                ->setTo($this->login)
                ->setFrom('hh.notify@yandex.ru')
                ->setSubject('Вы зарегистрированы')
                ->setHtmlBody("Ваш логин: {$this->login}<br> Ваш пароль: {$this->password}<br><a href='".Url::toRoute(['site/login'], true)."'>Войти в систему</a>")
                ->send();

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'position_id' => 'Должность',
            'name' => 'ФИО',
            'role' => 'Тип',
            'password_hash' => 'Password Hash',
            'password' => 'Пароль',
            'status' => 'Статус',
            'email' => 'Email',
            'is_deletable' => 'Удаляемый',
            'phone' => 'Телефон',
            'access' => 'Доступ'
        ];
    }

    /**
     * @return array
     */
    public static function roleLabels()
    {
        return [
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_MANAGER => 'Руководитель',
            self::ROLE_HR => 'Рекрутер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['accountable_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects0()
    {
        return $this->hasMany(Project::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks0()
    {
        return $this->hasMany(Task::className(), ['executor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
}
