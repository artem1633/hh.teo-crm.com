<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_stage".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $color Цвет
 * @property integer $sorting
 * @property int $as_default По умолчанию
 *
 * @property CandidateProjectStage[] $candidateProjectStages
 */
class ProjectStage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_stage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['as_default', 'sorting'], 'integer'],
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'color' => 'Цвет',
            'sorting' => 'Сортировка',
            'as_default' => 'По умолчанию',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateProjectStages()
    {
        return $this->hasMany(CandidateProjectStage::className(), ['stage_id' => 'id']);
    }
}
