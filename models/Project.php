<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property string $title Наименование
 * @property int $client_id Заказчик
 * @property string $date_start Дата начала
 * @property string $date_end Дата окончания
 * @property int $type_id Тип
 * @property int $status_id Статус
 * @property int $archive Архив
 * @property string $comment Комменатрий
 * @property int $accountable_user_id Ответственный пользователь
 * @property int $created_by
 * @property string $created_at
 *
 * @property CandidateProjectStage[] $candidateProjectStages
 * @property User $accountableUser
 * @property Client $client
 * @property User $createdBy
 * @property ProjectStatus $status
 * @property Project $type
 * @property Project[] $projects
 * @property ProjectFile[] $projectFiles
 * @property ProjectStep[] $projectSteps
 * @property Task[] $tasks
 */
class Project extends \yii\db\ActiveRecord
{
    public $files;

    /**
     * @var boolean
     */
    public $createTask;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['client_id', 'type_id', 'status_id', 'accountable_user_id', 'created_by', 'createTask', 'archive'], 'integer'],
            [['date_start', 'date_end', 'created_at', 'files'], 'safe'],
            [['comment'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['accountable_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['accountable_user_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'client_id' => 'Заказчик',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'type_id' => 'Тип',
            'status_id' => 'Статус',
            'comment' => 'Комменатрий',
            'accountable_user_id' => 'Ответственный пользователь',
            'createTask' => 'Создать задачу',
            'archive' => 'Архив',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert) {
            /** @var ProjectStage[] $stages */
            $stages = ProjectStage::find()->where(['as_default' => true])->all();

            foreach ($stages as $stage) {
                $step = new ProjectStep([
                    'project_id' => $this->id,
                    'name' => $stage->name,
                    'color' => $stage->color,
                    'sorting' => $stage->sorting,
                ]);
                $step->save(false);
            }
        }

        $files = explode(',', $this->files);

        foreach ($files as $file){
            if($file != null){
                $file = ProjectFile::findOne($file);
                $file->project_id = $this->id;
                $file->save(false);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateProjectStages()
    {
        return $this->hasMany(CandidateProjectStage::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountableUser()
    {
        return $this->hasOne(User::className(), ['id' => 'accountable_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectSteps()
    {
        return $this->hasMany(ProjectStep::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ProjectStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ProjectType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectFiles()
    {
        return $this->hasMany(ProjectFile::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['project_id' => 'id']);
    }
}
