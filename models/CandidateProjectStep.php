<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "candidate_project_step".
 *
 * @property int $id
 * @property int $candidate_id Кандидат
 * @property int $project_id Проект
 * @property int $step_id Этап
 *
 * @property Candidate $candidate
 * @property Project $project
 * @property ProjectStep $step
 */
class CandidateProjectStep extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate_project_step';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['candidate_id', 'project_id', 'step_id'], 'integer'],
            [['candidate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Candidate::className(), 'targetAttribute' => ['candidate_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['step_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectStep::className(), 'targetAttribute' => ['step_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'candidate_id' => 'Кандидат',
            'project_id' => 'Проект',
            'step_id' => 'Этап',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(Candidate::className(), ['id' => 'candidate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStep()
    {
        return $this->hasOne(ProjectStep::className(), ['id' => 'step_id']);
    }
}
