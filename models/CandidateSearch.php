<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Candidate;

/**
 * CandidateSearch represents the model behind the search form about `app\models\Candidate`.
 */
class CandidateSearch extends Candidate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'email', 'phone', 'city', 'comment', 'education', 'work_experience', 'age', 'languages', 'additional', 'doc_file', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Candidate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->orFilterWhere(['like', 'email', $this->name])
            ->orFilterWhere(['like', 'phone', $this->name])
            ->orFilterWhere(['like', 'city', $this->name])
            ->orFilterWhere(['like', 'comment', $this->name])
            ->orFilterWhere(['like', 'education', $this->name])
            ->orFilterWhere(['like', 'work_experience', $this->name])
            ->orFilterWhere(['like', 'age', $this->name])
            ->orFilterWhere(['like', 'languages', $this->name])
            ->orFilterWhere(['like', 'additional', $this->name])
            ->orFilterWhere(['like', 'doc_file', $this->name]);

        return $dataProvider;
    }
}
