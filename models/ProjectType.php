<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_type".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $color Цвет
 */
class ProjectType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'color' => 'Цвет',
        ];
    }

    /**
     * @return string
     */
    public function getRealColor()
    {
        return $this->color != null ? $this->color : '#cecece';
    }
}
