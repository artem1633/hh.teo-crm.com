<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "candidate_candidate_skill".
 *
 * @property int $id
 * @property int $candidate_id
 * @property int $candidate_skill_id
 *
 * @property Candidate $candidate
 * @property CandidateSkill $candidateSkill
 */
class CandidateCandidateSkill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate_candidate_skill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['candidate_id', 'candidate_skill_id'], 'integer'],
            [['candidate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Candidate::className(), 'targetAttribute' => ['candidate_id' => 'id']],
            [['candidate_skill_id'], 'exist', 'skipOnError' => true, 'targetClass' => CandidateSkill::className(), 'targetAttribute' => ['candidate_skill_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'candidate_id' => 'Candidate ID',
            'candidate_skill_id' => 'Candidate Skill ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(Candidate::className(), ['id' => 'candidate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateSkill()
    {
        return $this->hasOne(CandidateSkill::className(), ['id' => 'candidate_skill_id']);
    }
}
