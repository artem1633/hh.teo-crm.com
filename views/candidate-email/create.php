<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CandidateEmail */

?>
<div class="candidate-email-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
