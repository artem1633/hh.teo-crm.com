<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CandidateEmail */
?>
<div class="candidate-email-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'candidate_id',
            'text:ntext',
            'sent',
            'created_at',
        ],
    ]) ?>

</div>
