<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'candidate_id',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'text',
        'content' => function($data){
            $html = '<span class="msg-text" style="display: none;">'.$data->text.'</span>'.Html::a('Показать', '#', ['onclick' => '
                event.preventDefault();
                $(this).hide();
                $(this).parent().find(".msg-text").slideDown();
            ', 'style' => 'color: #a8a8a8; display: inline-block; text-decoration: underline;']);

            return $html;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'label' => 'Время',
        'format' => ['date', 'php:d M Y H:i:s'],
    ],

];   