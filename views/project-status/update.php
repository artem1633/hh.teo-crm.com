<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectStatus */
?>
<div class="project-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
