<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\forms\DocxForm;

/* @var $this yii\web\View */
/* @var $model app\models\Candidate */
/* @var $form yii\widgets\ActiveForm */


?>

    <div class="candidate-form">

        <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-md-12">

                            <?= \kato\DropZone::widget([
                                'id'        => 'dzImage', // <-- уникальные id
                                'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'project_id' => $model->id]),
                                'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                                'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                                'options' => [
                                    'maxFilesize' => '2',
                                ],
                                'clientEvents' => [
                                    'success' => "function(file, response){ 
                                        $.pjax.reload('#crud-file-datatable-pjax');                                    
                                    }",
                                ],
                            ]);?>
                    </div>
                </div>



        <?php ActiveForm::end(); ?>

    </div>

