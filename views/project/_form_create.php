<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $task app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a data-toggle="tab" href="#tab-project">Вакансия</a></li>
                <li><a data-toggle="tab" href="#tab-task">Задача</a></li>
            </ul>
        </div>
    </div>

    <div class="tab-content">
        <div id="tab-project" class="tab-pane fade in active">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'client_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Client::find()->all(), 'id', 'name')) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'date_start')->input('date') ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'date_end')->input('date') ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'type_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\ProjectType::find()->all(), 'id', 'name')) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'status_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\ProjectStatus::find()->all(), 'id', 'name')) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'accountable_user_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name')) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php if($model->isNewRecord): ?>
                        <?= \kato\DropZone::widget([
                            'id'        => 'dzImage', // <-- уникальные id
                            'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file' ]),
                            'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                            'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                            'options' => [
                                'maxFilesize' => '2',
                            ],
                            'clientEvents' => [
                                'success' => "function(file, response){ 
                                response = JSON.parse(response);
                                   
                                var id = response.record.id;
                                
                                var value = $('#project-files').val();
                                $('#project-files').val(value+','+id);
                             }",
                                'complete' => "function(file){ console.log(file); }",
                            ],
                        ]);?>
                        <div class="hidden">
                            <?= $form->field($model, 'files')->hiddenInput() ?>
                        </div>
                    <?php else: ?>
                        <?= \kato\DropZone::widget([
                            'id'        => 'dzImage', // <-- уникальные id
                            'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'project_id' => $model->id]),
                            'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                            'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                            'options' => [
                                'maxFilesize' => '2',
                            ],
                        ]);?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div id="tab-task" class="tab-pane fade in">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'createTask')->checkbox() ?>
                </div>
            </div>
            <div id="task-wrapper" style="display: none;">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($task, 'executor_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'login')) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($task, 'candidate_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Candidate::find()->all(), 'id', 'name')) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($task, 'executed_at')->widget(\kartik\datetime\DateTimePicker::className(), [

                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($task, 'remember_time')->input('number') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($task, 'status_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\TaskStatus::find()->all(), 'id', 'name')) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($task, 'type_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\TaskType::find()->all(), 'id', 'name')) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($task, 'text')->textarea(['rows' => 6]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($task, 'result_text')->textarea(['rows' => 6]) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>



    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS
$('#project-createtask').change(function(){
    if($(this).is(':checked')){
        $('#task-wrapper').slideDown('fast');
    } else {
        $('#task-wrapper').slideUp('fast');
    }
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
