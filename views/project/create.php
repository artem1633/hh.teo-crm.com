<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $task app\models\Task */

?>
<div class="project-create">
    <?= $this->render('_form_create', [
        'model' => $model,
        'task' => $task
    ]) ?>
</div>
