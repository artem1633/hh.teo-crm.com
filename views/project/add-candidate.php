<?php

use yii\widgets\ActiveForm;

/**
 * @var $model \app\models\CandidateProjectStep
 */

?>

<?php $form = ActiveForm::begin() ?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'candidate_id')->widget(\kartik\select2\Select2::className(), [
            'data' => \yii\helpers\ArrayHelper::map($candidates, 'id', 'name'),
        ]) ?>
    </div>
</div>

<?php ActiveForm::end() ?>
