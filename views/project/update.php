<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
?>
<div class="project-update">

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
