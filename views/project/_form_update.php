<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'client_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Client::find()->all(), 'id', 'name')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'date_start')->input('date') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'date_end')->input('date') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'type_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\ProjectType::find()->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\ProjectStatus::find()->all(), 'id', 'name')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'accountable_user_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php if($model->isNewRecord): ?>
                <?= \kato\DropZone::widget([
                    'id'        => 'dzImage', // <-- уникальные id
                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file' ]),
                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                    'options' => [
                        'maxFilesize' => '2',
                    ],
                    'clientEvents' => [
                        'success' => "function(file, response){ 
                                response = JSON.parse(response);
                                   
                                var id = response.record.id;
                                
                                var value = $('#project-files').val();
                                $('#project-files').val(value+','+id);
                             }",
                        'complete' => "function(file){ console.log(file); }",
                    ],
                ]);?>
                <div class="hidden">
                    <?= $form->field($model, 'files')->hiddenInput() ?>
                </div>
            <?php else: ?>
                <?= \kato\DropZone::widget([
                    'id'        => 'dzImage', // <-- уникальные id
                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'project_id' => $model->id]),
                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                    'options' => [
                        'maxFilesize' => '2',
                    ],
                ]);?>
            <?php endif; ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
