<?php

use yii\widgets\ActiveForm;

/**
 * @var \app\models\forms\EmailForm $model
 * @var integer $projectId
 * @var integer $candidateId
 */

?>

<?php $form = ActiveForm::begin() ?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'templateId')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\EmailTemplate::find()->all(), 'id', 'name'), ['prompt' => 'Выберите шаблон']) ?>
    </div>
</div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'message')->widget(\mihaildev\ckeditor\CKEditor::className(), [
                'editorOptions' => [
                    'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                ]
            ]) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>



<?php

$script = <<< JS

$('#emailform-templateid').change(function(){
    var templateId = $(this).val();
    $.ajax({
        url: '/project/get-email-template-text?candidateId={$candidateId}&projectId={$projectId}&templateId='+templateId,
        method: 'GET',
        success: function(response){
            if(response.message != undefined){
                CKEDITOR.instances['emailform-message'].setData(response.message);
            }
        }
    });
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>