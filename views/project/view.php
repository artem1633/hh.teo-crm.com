<?php

use kartik\sortable\Sortable;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $steps \app\models\ProjectStep[] */

\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->params['minified-menu'] = true;

$this->title = "Вакансия «{$model->title}»";

?>
<div class="project-view">

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse" style="margin-top: -35px;"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'title',
                            [
                                'attribute' => 'client_id',
                                'value' => function($client){
                                    $client = \app\models\Client::findOne($client->id);
                                    if($client){
                                        return $client->name;
                                    }
                                }
                            ],
                            'date_start',
                            'date_end',
                            [
                                'attribute' => 'type_id',
                                'value' => function($model){
                                    $type = \app\models\ProjectType::findOne($model->type_id);
                                    if($type){
                                        return $type->name;
                                    }
                                }
                            ],
                            [
                                'attribute' => 'status_id',
                                'value' => function($model){
                                    $status = \app\models\ProjectStatus::findOne($model->status_id);
                                    if($status){
                                        return $status->name;
                                    }
                                },
                            ],
                            'comment:ntext',
                            [
                                'attribute' => 'accountable_user_id',
                                'value' => function($model){
                                    $user = \app\models\User::findOne($model->accountable_user_id);
                                    if($user){
                                        return $user->name;
                                    }
                                },
                            ],
                            [
                                'attribute' => 'created_at',
                                'label' => 'Дата и время создания'
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Файлы</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <?= $this->render('@app/views/project-file/index', [
                        'searchModel' => $filesSearchModel,
                        'dataProvider' => $filesDataProvider,
                        'projectId' => $model->id
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Задачи</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <?= $this->render('@app/views/task/index-project', [
                        'searchModel' => $tasksSearchModel,
                        'dataProvider' => $tasksDataProvider,
                        'projectId' => $model->id
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <?php Pjax::begin(['id' => 'panel-pjax', 'enablePushState' => false]) ?>
    <div class="panel panel-inverse black-back">
        <div class="panel-heading" style="background: #fafafa;">
            <h4 class="panel-title" style="color: #000;"><?=$model->title?></h4>
            <div class="panel-heading-btn">
                <?= Html::a('Добавить этап', ['project-step/create', 'projectId' => $model->id, 'pjaxContainer' => 'panel-pjax'], ['class' => 'btn btn-success btn-sm', 'role' => 'modal-remote', 'style' => 'margin-top: -37px;']); ?>
            </div>
        </div>
        <div class="panel-body no-before theme-scroll" style="padding: 0; overflow-y: auto; width: 100%; background: #e0e0e0;">
            <div class="desc-page-container" id="boards-sprint-item-ajax" style="white-space: nowrap; padding-left: 12px;">
                <div class="row">
                    <?php foreach($steps as $step): ?>
                        <?php
                        $stepId = $step->id;
                        $items = [];
                        $candidatesPks = \yii\helpers\ArrayHelper::getColumn(\app\models\CandidateProjectStep::find()->where(['project_id' => $model->id, 'step_id' => $step->id])->all(), 'candidate_id');

                        $candidates = \app\models\Candidate::find()->where(['id' => $candidatesPks])->all();

                        foreach ($candidates as $candidate){
                            $items[]['content'] = '
                                         <div class="panel " style="margin-bottom:1px;" id="'.$candidate->id.'">                            
                                        <div class="panel-heading" >
                                            <a href="#">
                                            <div class="panel-heading-btn" style="display: none;">
                                                <a href="'.\yii\helpers\Url::toRoute(['candidate/delete-from-project', 'projectId' => $model->id, 'candidateId' => $candidate->id, 'pjaxContainer' => 'column-pjax-'.$step->id]).'" role="modal-remote" title="Удалить" data-request-method="post" data-confirm-title="Вы уверены?" data-confirm-message="Вы действительно хотите удалить данного кандидата из проекта?" style="font-size: 14px;">
                                                    <i class="fa fa-trash text-danger" aria-hidden="true"></i>
                                                </a>
                                                <a title="Перейти на страницу кандидату" href="'.\yii\helpers\Url::toRoute(['candidate/view', 'id' => $candidate->id]).'" role="modal-remote" style="font-size: 14px;">
                                                    <i class="fa fa-eye text-success" aria-hidden="true"></i>
                                                </a>
                                                <a title="Просмотр" href="'.\yii\helpers\Url::toRoute(['candidate/view', 'id' => $candidate->id]).'" data-pjax="0" style="font-size: 14px;">
                                                    <i class="fa fa-arrow-right text-primary" aria-hidden="true"></i>
                                                </a>
                                                <a href="'.\yii\helpers\Url::toRoute(['project/send-sms', 'projectId' => $model->id, 'candidateId' => $candidate->id, 'pjaxContainer' => 'column-pjax-'.$step->id]).'" role="modal-remote" title="Отправить SMS" style="font-size: 14px;">
                                                    <i class="fa fa-mobile text-warning" aria-hidden="true"></i>
                                                </a>
                                                <a href="'.\yii\helpers\Url::toRoute(['project/send-email', 'projectId' => $model->id, 'candidateId' => $candidate->id, 'pjaxContainer' => 'column-pjax-'.$step->id]).'" role="modal-remote" title="Отправить Email" style="font-size: 14px;">
                                                    <i class="fa fa-envelope text-warning" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div class="panel-heading-btn" style="position: relative">
                                            
                                              <button type="button" class="btn btn-primary btn-circle btn-xs" onclick="$(this).parent().find(\'.dropdown-menu\').toggle()"><i class="fa fa-arrow-down"></i></button>
                                              <ul class="dropdown-menu" style="height: auto; position: absolute; left: -134px; top: 26px; padding: 0;">
                                                <li>
                                                    <a href="'.\yii\helpers\Url::toRoute(['candidate/delete-from-project', 'projectId' => $model->id, 'candidateId' => $candidate->id, 'pjaxContainer' => 'column-pjax-'.$step->id]).'" role="modal-remote" title="Удалить" data-request-method="post" data-confirm-title="Вы уверены?" data-confirm-message="Вы действительно хотите удалить данного кандидата из проекта?" style="font-size: 13px;">
                                                        <i class="fa fa-trash text-danger" aria-hidden="true"></i>
                                                        Удалить
                                                    </a>
                                                </li>
                                                <li>
                                                    <a title="Перейти на страницу кандидату" href="'.\yii\helpers\Url::toRoute(['candidate/view', 'id' => $candidate->id]).'" role="modal-remote" style="font-size: 13px;">
                                                        <i class="fa fa-eye text-success" aria-hidden="true"></i>
                                                        Кандидат
                                                    </a>
                                                </li>
                                                <li>
                                                    <a title="Просмотр" href="'.\yii\helpers\Url::toRoute(['candidate/view', 'id' => $candidate->id]).'" data-pjax="0" style="font-size: 13px;">
                                                        <i class="fa fa-arrow-right text-primary" aria-hidden="true"></i>
                                                        Просмотр
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="'.\yii\helpers\Url::toRoute(['project/send-sms', 'projectId' => $model->id, 'candidateId' => $candidate->id, 'pjaxContainer' => 'column-pjax-'.$step->id]).'" role="modal-remote" title="Отправить SMS" style="font-size: 13px;">
                                                        <i class="fa fa-mobile text-warning" aria-hidden="true"></i>
                                                        Отправить СМС
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="'.\yii\helpers\Url::toRoute(['project/send-email', 'projectId' => $model->id, 'candidateId' => $candidate->id, 'pjaxContainer' => 'column-pjax-'.$step->id]).'" role="modal-remote" title="Отправить Email" style="font-size: 13px;">
                                                        <i class="fa fa-envelope text-warning" aria-hidden="true"></i>
                                                        Отправить письмо
                                                    </a>
                                                </li>
                                              </ul>
                                            
                                              
                                            
                                            </div>
                                            <h6 class="panel-title" style="font-size: 11px;"><b>'.$candidate->name.'</b></h6>
</a>
                                        </div>
                                    </div>
                                    ';
                        }
                        ?>
                        <div class="step-column" id="<?=$step->id?>">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'column-pjax-'.$step->id]) ?>
                            <div id="second-pjax" data-pjax-container="" data-pjax-timeout="1000">
                                <div class="step-title">

                                    <div class="bold-title" style="font-size: 12px; margin-top: 5px;">
                                        <?=$step->name?>
                                        <?= Html::a('<i class="fa fa-plus"></i>', ['add-candidate', 'projectId' => $model->id, 'stepId' => $step->id, 'pjaxContainer' => 'column-pjax-'.$step->id], ['class' => 'pull-right', 'title' => 'Добавить кандидата', 'role' => 'modal-remote']) ?>

                                        <a href="/project-step/update?id=<?=$step->id?>&pjaxContainer=column-pjax-<?=$step->id?>" title="Изменить" role="modal-remote" data-request-method="post"><i class="fa fa-pencil text-primary" style="font-size: 16px;"></i></a>
                                        <a href="/project-step/delete?id=<?=$step->id?>&pjaxContainer=column-pjax-<?=$step->id?>" title="Удалить" role="modal-remote" data-request-method="post" data-confirm-title="Вы уверены?" data-confirm-message="Вы действительно хотите удалить данную запись?"><i class="fa fa-trash text-danger" style="font-size: 16px;"></i></a> </div>

                                    <span class="stagevalue">
                               <span class="value">
                                   <small>Кандидаты <span class="count-clients-item"><?=count($items)?></span></small>
                                </span>
                                            <div style="width: 100%; height:7px;" class="progress progress-striped progress-sm active m-t-5">
                                                <div class="progress-bar progress-bar-success" style="width:100%; background-color: <?=$step->getRealColor()?>;">
                                                </div>
                                            </div>
                                </span>
                                </div>
                            </div>
                            <?php

                            echo Sortable::widget([
                                'connected'=>true,
                                'options' => [
                                    'data-step'=>$stepId,
                                    'class' => 'theme-scroll',
//            'data-url-update' => Url::to(['ajax/update-step-client']),
                                ],
                                'items' => $items,
                                'pluginEvents' => [
                                    'sortupdate' => "function(e, e2) {
                                var currentElementSelector = $(e2.item).find('div');
                                
                                var currentStep = {$stepId};
                                var currentId = currentElementSelector.attr('id');                                
                                var oldStepId =  $(currentElementSelector).data('parent-step');
                                
                                currentElementSelector.data('parent-step', currentStep + 'step');
                                                                                             
                                //Этап from
                                var oldStepObj = $('#' + oldStepId);
                                
                                var countClientsItemFrom = oldStepObj.find('.count-clients-item');
                                var countClientsItemFromValue = countClientsItemFrom.text();
                                countClientsItemFromValue--;
                                countClientsItemFrom.text(countClientsItemFromValue);
                                                              
                                //Этап to
                                var currentStepObj = $('#' + currentStep + 'step');
                                
                                var countClientsItemTo = currentStepObj.find('.count-clients-item');
                                var countClientsItemToValue = countClientsItemTo.text();
                                countClientsItemToValue++;
                                countClientsItemTo.text(countClientsItemToValue);
                                
                                console.log('currentStep', currentStep);
                                console.log('currentId', currentId);
                                
                                $.ajax({ type: 'GET',
                                    url: '/project/update-step?projectId={$model->id}&stepId='+currentStep+'&candidateId='+currentId,
                                    success: function(data){
                                        console.log(data);
                                        console.log(currentId);
                                        console.log(currentStep);
                                    }
                                });
                            }",
                                ],
                            ]);
                            ?>
                            <?php Pjax::end() ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end() ?>

</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => [
        'tabindex' => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

