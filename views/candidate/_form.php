<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\forms\DocxForm;

/* @var $this yii\web\View */
/* @var $model app\models\Candidate */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false) {
    $model->tags = \yii\helpers\ArrayHelper::getColumn(\app\models\CandidateCandidateTag::find()->where(['candidate_id' => $model->id])->all(), 'candidate_tag_id');
    $model->tags = \yii\helpers\ArrayHelper::getColumn(\app\models\CandidateTag::find()->where(['id' => $model->tags])->all(), 'name');
    $model->skills = \yii\helpers\ArrayHelper::getColumn(\app\models\CandidateCandidateSkill::find()->where(['candidate_id' => $model->id])->all(), 'candidate_skill_id');
    $model->skills = \yii\helpers\ArrayHelper::getColumn(\app\models\CandidateSkill::find()->where(['id' => $model->skills])->all(), 'name');
}


?>

<div class="candidate-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <?php $modelDocx = new DocxForm(); ?>
                    <?= $form->field($modelDocx, 'file')->fileInput()->label('Docx Файл') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'age')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'tags')->widget(\kartik\select2\Select2::className(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\CandidateTag::find()->all(), 'name', 'name'),
                        'options' => [
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [','],
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'skills')->widget(\kartik\select2\Select2::className(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\CandidateSkill::find()->all(), 'name', 'name'),
                        'options' => [
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [','],
                        ],
                    ]) ?>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <?php if($model->isNewRecord): ?>
                        <?= \kato\DropZone::widget([
                            'id'        => 'dzImage', // <-- уникальные id
                            'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file' ]),
                            'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                            'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                            'options' => [
                                'maxFilesize' => '2',
                            ],
                            'clientEvents' => [
                                'success' => "function(file, response){ 
                                response = JSON.parse(response);
                                   
                                var id = response.record.id;
                                
                                var value = $('#candidate-files').val();
                                $('#candidate-files').val(value+','+id);
                             }",
                                'complete' => "function(file){ console.log(file); }",
                            ],
                        ]);?>
                    <?php else: ?>
                        <?= \kato\DropZone::widget([
                            'id'        => 'dzImage', // <-- уникальные id
                            'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'candidate_id' => $model->id]),
                            'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                            'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                            'options' => [
                                'maxFilesize' => '2',
                            ],
                        ]);?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="hidden">
                <?= $form->field($model, 'files')->hiddenInput() ?>
            </div>

        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'education')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'work_experience')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'languages')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'additional')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>

    </div>



	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>


<?php

$script = <<< JS
$('#docxform-file').change(function(){
    var file = $('#docxform-file').prop('files')[0];
    var formData = new FormData();
    formData.append('DocxForm[file]', file);
    $.ajax({
        method: "POST",
        url: '/candidate/parse-docx-file',
        data: formData,
        processData: false,
        contentType: false,
        success: function(response){
            $('#candidate-name').val(response.name);
            $('#candidate-work_experience').val(response.work);
            $('#candidate-age').val(response.age);
            $('#candidate-phone').val(response.phone);
            $('#candidate-education').val(response.education);
            $('#candidate-email').val(response.email);
            $('#candidate-additional').val(response.additional);
            for(var i = 0; i < response.skills.length; i++)
            {
                console.log(response.skills[i]);
                var option = new Option(response.skills[i], response.skills[i], false, true);
                $('#candidate-skills').append(option).trigger('change');
            }
        }
    });
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>