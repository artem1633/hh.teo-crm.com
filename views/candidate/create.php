<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Candidate */

?>
<div class="candidate-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
