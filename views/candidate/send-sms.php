<?php

use yii\widgets\ActiveForm;

/**
 * @var \app\models\forms\SmsForm $model
 * @var integer $candidateId
 */

?>

<?php $form = ActiveForm::begin() ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'templateId')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\SmsTemplate::find()->all(), 'id', 'name'), ['prompt' => 'Выберите шаблон']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'message')->textarea(['rows' => 9]) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>


<?php

$script = <<< JS

$('#smsform-templateid').change(function(){
    var templateId = $(this).val();
    $.ajax({
        url: '/candidate/get-sms-template-text?candidateId={$candidateId}&templateId='+templateId,
        method: 'GET',
        success: function(response){
            if(response.message != undefined){
                $('#smsform-message').val(response.message);
            }
        }
    });
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>