<?php

use yii\bootstrap\Modal;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Candidate */

$this->title = "Кандидат {$model->name}";

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>
<div class="candidate-view">
 
<div class="row">
    <div class="col-md-7">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Информация</h4>
            </div>
            <div class="panel-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'email:email',
                        'phone',
                        'city',
                        'comment:ntext',
                        'education:ntext',
                        'work_experience:ntext',
                        'age',
                        'languages:ntext',
                        'additional:ntext',
                        'doc_file:ntext',
                        'created_at',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">СМС сообщения</h4>
                <div class="panel-heading-btn" style="margin-top: -20px;">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <?= $this->render('@app/views/candidate-sms/index', [
                    'searchModel' => $smsSearchModel,
                    'dataProvider' => $smsDataProvider,
                    'candidateId' => $model->id
                ]) ?>
            </div>
        </div>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Email сообщения</h4>
                <div class="panel-heading-btn" style="margin-top: -20px;">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <?= $this->render('@app/views/candidate-email/index', [
                    'searchModel' => $emailSearchModel,
                    'dataProvider' => $emailDataProvider,
                    'candidateId' => $model->id,
                ]) ?>
            </div>
        </div>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Файлы</h4>
                <div class="panel-heading-btn" style="margin-top: -20px;">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <?= $this->render('@app/views/candidate-file/index', [
                    'searchModel' => $filesSearchModel,
                    'dataProvider' => $filesDataProvider,
                    'candidateId' => $model->id
                ]) ?>
            </div>
        </div>
    </div>
</div>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
