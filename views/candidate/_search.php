<?php
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\forms\DocxForm;

/* @var $this yii\web\View */
/* @var $model app\models\CandidateSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $data \app\models\Candidate[] */

$data = \yii\helpers\ArrayHelper::getColumn($data, 'name');

?>

    <div class="candidate-form pull-right" style="display: inline-block; margin-top: -4px; width: 25%;">

        <?php $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'GET']); ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'name')->widget(TypeaheadBasic::className(), [
                    'data' => $data,
                    'options' => [
                        'placeholder' => 'Поиск',
                        'autocomplete' => 'off',
                    ],
                ])->label(false) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


<?php
$script = <<< JS

$('#candidatesearch-name').change(function(){
    $('#search-form').submit();
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>