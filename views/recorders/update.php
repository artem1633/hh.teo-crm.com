<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recorders */
?>
<div class="recorders-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
