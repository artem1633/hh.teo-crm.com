<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Recorders */

?>
<div class="recorders-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
