<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TaskFile */

?>
<div class="task-file-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
