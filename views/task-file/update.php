<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskFile */
?>
<div class="task-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
