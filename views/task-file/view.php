<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaskFile */
?>
<div class="task-file-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'task_id',
            'name',
            'path',
            'created_at',
        ],
    ]) ?>

</div>
