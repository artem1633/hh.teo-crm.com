<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmsTemplate */

?>
<div class="sms-template-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
