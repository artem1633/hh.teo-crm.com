<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SmsTemplate */
?>
<div class="sms-template-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'content:ntext',
        ],
    ]) ?>

</div>
