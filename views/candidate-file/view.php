<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CandidateFile */
?>
<div class="candidate-file-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'candidate_id',
            'name',
            'path',
            'created_at',
        ],
    ]) ?>

</div>
