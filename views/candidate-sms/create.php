<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CandidateSms */

?>
<div class="candidate-sms-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
