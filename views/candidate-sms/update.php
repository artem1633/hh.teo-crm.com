<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CandidateSms */
?>
<div class="candidate-sms-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
