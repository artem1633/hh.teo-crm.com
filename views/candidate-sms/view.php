<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CandidateSms */
?>
<div class="candidate-sms-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'candidate_id',
            'text:ntext',
            'sent',
            'created_at',
        ],
    ]) ?>

</div>
