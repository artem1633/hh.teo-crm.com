<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CandidateSmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $candidateId integer */

CrudAsset::register($this);

?>
            <?=GridView::widget([
            'id'=>'crud-sms-datatable',
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'rowOptions' => function($data){
                if($data->sent == 1){
                    return ['class' => 'success'];
                } else {
                    return ['class' => 'danger'];
                }
            },
            'panelBeforeTemplate' => Html::a('Отправить СМС', ['candidate/send-sms', 'candidateId' => $candidateId, 'pjaxContainer' => '#crud-sms-datatable-pjax'], ['class' => 'btn btn-info btn-sm', 'role' => 'modal-remote']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ])?>
