<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProjectType */

?>
<div class="project-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
