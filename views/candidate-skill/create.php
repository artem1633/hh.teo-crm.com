<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CandidateSkill */

?>
<div class="candidate-skill-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
