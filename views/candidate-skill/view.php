<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CandidateSkill */
?>
<div class="candidate-skill-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
