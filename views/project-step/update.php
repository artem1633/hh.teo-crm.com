<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectStep */
?>
<div class="project-step-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
