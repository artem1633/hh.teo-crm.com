<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskType */
?>
<div class="task-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
