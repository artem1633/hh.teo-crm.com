<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CandidateFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projectId integer */

//$this->title = "Файлы кандидата";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
            <?=GridView::widget([
            'id'=>'crud-file-datatable',
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' =>
                Html::a('Добавить <i class="fa fa-plus"></i>', ['add-file', 'id' => $projectId],
                    ['role'=>'modal-remote','title'=> 'Добавить файл','class'=>'btn btn-success']).'&nbsp;'.
                Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>null,
            ]
            ])?>

