<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CandidateFile */
?>
<div class="candidate-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
