<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Persons */

?>
<div class="persons-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
