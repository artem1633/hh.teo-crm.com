<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Persons */
?>
<div class="persons-view">

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                </div>
                <div class="panel-body">
                    <p class="text-center">
                        <img src="/<?= $model->avatar ?>" style="object-fit: cover; border: 2px solid #e1e1e1; max-width: 100%; height: 300px;" alt="">
                    </p>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'name',
                            [
                                'attribute' => 'home_address',
                                'format' => 'html',
                                'value' => function($model){
                                    if($model->home_address_is_confirmed == 1){
                                        return $model->home_address;
                                    } else {
                                        return "<span style='color: darkgray;'>{$model->home_address}</span>";
                                    }
                                }
                            ],
                            [
                                'attribute' => 'date_born',
                                'format' => ['date', 'php:'.Yii::$app->params['dateFormat']],
                            ],
                            [
                                'attribute' => 'date_dead',
                                'visible' => $model->date_dead != null,
                                'format' => ['data',  'php:'.Yii::$app->params['dateFormat']],
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
