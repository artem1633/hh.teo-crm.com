<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CandidateTag */
?>
<div class="candidate-tag-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
