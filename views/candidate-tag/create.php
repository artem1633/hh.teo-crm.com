<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CandidateTag */

?>
<div class="candidate-tag-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
