<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
/* @var $projectId integer */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($projectId == null): ?>
        <?= $form->field($model, 'project_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'title')) ?>
    <?php endif; ?>

    <?= $form->field($model, 'executor_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'login')) ?>

    <?= $form->field($model, 'candidate_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Candidate::find()->all(), 'id', 'name'), [
            'prompt' => 'Выберите кандидата',
    ]) ?>

    <?= $form->field($model, 'executed_at')->widget(\kartik\datetime\DateTimePicker::className(), [

    ]) ?>

    <?= $form->field($model, 'remember_time')->input('number') ?>

    <?= $form->field($model, 'status_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\TaskStatus::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'type_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\TaskType::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'result_text')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-md-12">
            <?php if($model->isNewRecord): ?>
                <?= \kato\DropZone::widget([
                    'id'        => 'dzImage', // <-- уникальные id
                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'task/upload-file' ]),
                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                    'options' => [
                        'maxFilesize' => '2',
                    ],
                    'clientEvents' => [
                        'success' => "function(file, response){ 
                                response = JSON.parse(response);
                                   
                                var id = response.record.id;
                                
                                var value = $('#task-files').val();
                                $('#task-files').val(value+','+id);
                             }",
                        'complete' => "function(file){ console.log(file); }",
                    ],
                ]);?>
                <div class="hidden">
                    <?= $form->field($model, 'files')->hiddenInput() ?>
                </div>
            <?php else: ?>
                <?= \kato\DropZone::widget([
                    'id'        => 'dzImage', // <-- уникальные id
                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'task/upload-file', 'task_id' => $model->id]),
                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                    'options' => [
                        'maxFilesize' => '2',
                    ],
                ]);?>
            <?php endif; ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
