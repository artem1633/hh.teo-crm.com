<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projectId integer */

$this->title = "Задачи";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse task-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Задачи</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => [
                [
                    'class' => 'kartik\grid\CheckboxColumn',
                    'width' => '20px',
                ],
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                // [
                // 'class'=>'\kartik\grid\DataColumn',
                // 'attribute'=>'id',
                // ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'project_id',
                    'value' => 'project.title',
                    'visible' => $projectId == null
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'executor_id',
                    'value' => 'executor.name',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'candidate_id',
                    'value' => 'candidate.name',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'executed_at',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'remember_time',
                ],
                // [
                // 'class'=>'\kartik\grid\DataColumn',
                // 'attribute'=>'status_id',
                // ],
                // [
                // 'class'=>'\kartik\grid\DataColumn',
                // 'attribute'=>'type_id',
                // ],
                // [
                // 'class'=>'\kartik\grid\DataColumn',
                // 'attribute'=>'text',
                // ],
                // [
                // 'class'=>'\kartik\grid\DataColumn',
                // 'attribute'=>'result_text',
                // ],
                // [
                // 'class'=>'\kartik\grid\DataColumn',
                // 'attribute'=>'created_by',
                // ],
                // [
                // 'class'=>'\kartik\grid\DataColumn',
                // 'attribute'=>'created_at',
                // ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'urlCreator' => function($action, $model, $key, $index) use ($projectId){
                        return Url::to([$action,'id'=>$key, 'projectId' => $projectId]);
                    },
                    'template' => '{view} {update}{delete}',
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                                'role'=>'modal-remote', 'title'=>'Удалить',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Вы уверены?',
                                'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                                    'role'=>'modal-remote', 'title'=>'Изменить',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                ])."&nbsp;";
                        }
                    ],
                ],

            ],
            'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Добавить задачу','class'=>'btn btn-success']).'&nbsp;'.
                Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>BulkButtonWidget::widget([
            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
            ["bulk-delete"] ,
            [
            "class"=>"btn btn-danger btn-xs",
            'role'=>'modal-remote-bulk',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
            ]),
            ]).
            '<div class="clearfix"></div>',
            ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
