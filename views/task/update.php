<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $projectId integer */
?>
<div class="task-update">

    <?= $this->render('_form', [
        'model' => $model,
        'projectId' => $projectId,
    ]) ?>

</div>
