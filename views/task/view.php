<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
?>
<div class="task-view">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'project_id',
                    'executor_id',
                    'candidate_id',
                    'executed_at',
                    'remember_time:datetime',
                    'status_id',
                    'type_id',
                    'text:ntext',
                    'result_text:ntext',
                    'created_by',
                    'created_at',
                ],
            ]) ?>
        </div>
    </div>

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Файлы</h4>
        </div>
        <div class="panel-body">
            <?= $this->render('@app/views/task-file/index', [
                'searchModel' => $filesSearchModel,
                'dataProvider' => $filesDataProvider,
            ]) ?>
        </div>
    </div>

</div>
