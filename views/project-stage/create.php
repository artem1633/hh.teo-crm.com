<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProjectStage */

?>
<div class="project-stage-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
