<?php

/** @var $this yii\web\View */

\app\assets\YandexMapsAsset::register($this);

?>

<div id="map" style="width: 2000px; height: 800px"></div>

<script>
    var Map = null;
    $(document).ready(function(){
        ymaps.ready(init);
        function init(){
            // Создание карты.
            Map = new ymaps.Map("map", {
                // Координаты центра карты.
                // Порядок по умолчнию: «широта, долгота».
                // Чтобы не определять координаты центра карты вручную,
                // воспользуйтесь инструментом Определение координат.
                center: [55.76, 37.64],
                // Уровень масштабирования. Допустимые значения:
                // от 0 (весь мир) до 19.
                zoom: 7
            });

            var userIconContentLayout = ymaps.templateLayoutFactory.createClass('<div class="placemark_layout_container"><div class="circle_layout"><i style="font-size: 20px;" class="fa fa-user text-success"></i></div></div>');
            var usersIconContentLayout = ymaps.templateLayoutFactory.createClass('<div class="placemark_layout_container"><div class="circle_layout"><i style="font-size: 20px;" class="fa fa-users text-success"></i></div></div>');
            var userNotConfirmedAddressIconContentLayout = ymaps.templateLayoutFactory.createClass('<div class="placemark_layout_container"><div class="circle_layout"><i style="font-size: 20px; color: darkgray;" class="fa fa-user"></i></div></div>');
            var organizationIconContentLayout = ymaps.templateLayoutFactory.createClass('<div class="placemark_layout_container"><div class="circle_layout"><i style="font-size: 20px;" class="fa fa-building text-primary"></i></div></div>');

            $.get('http://observer.loc/map/persons', function(persons){
                $.each(persons, function(){
                    var person = this;
                    ymaps.geocode(this.home_address).then(function(res){
                        var coor = res.geoObjects.get(0).geometry.getCoordinates();

                        var icon = userIconContentLayout;

                        if(person.home_address_is_confirmed == 1){
                            var icon = userIconContentLayout;
                        }

                        if(person.home_address_is_confirmed == 0){
                            var icon = userNotConfirmedAddressIconContentLayout;
                        }

                        var mark = new ymaps.Placemark(
                            coor,
                            {
                                hintContent: person.name,
                            }, {
                                iconLayout: icon,
                                // Описываем фигуру активной области
                                // "Прямоугольник".
                                iconShape: {
                                    type: 'Circle',
                                    // Круг описывается в виде центра и радиуса
                                    coordinates: [0, 0],
                                    radius: 25
                                }
                            }
                        );

                        Map.geoObjects.add(mark);
                    }, function(err){
                    });
                });
            });

            $.get('http://observer.loc/map/organizations', function(organizations){
                $.each(organizations, function(){
                    var organization = this;
                    ymaps.geocode(this.address).then(function(res){
                        var coor = res.geoObjects.get(0).geometry.getCoordinates();

                        var icon = organizationIconContentLayout;

                        var mark = new ymaps.Placemark(
                            coor,
                            {
                                hintContent: organization.title,
                            }, {
                                iconLayout: icon,
                                // Описываем фигуру активной области
                                // "Прямоугольник".
                                iconShape: {
                                    type: 'Circle',
                                    // Круг описывается в виде центра и радиуса
                                    coordinates: [0, 0],
                                    radius: 25
                                }
                            }
                        );

                        Map.geoObjects.add(mark);
                    }, function(err){
                    });
                });
            });
        }
    });
</script>
