<?php

use yii\helpers\Url;

?>

<div id="sidebar" class="sidebar">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\Menu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Вакансии', 'icon' => 'fa  fa-th', 'url' => ['/project'],],
                    ['label' => 'Кандидаты', 'icon' => 'fa  fa-users', 'url' => ['/candidate'],],
                    ['label' => 'Задачи', 'icon' => 'fa  fa-flag', 'url' => ['/task'],],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Настройки', 'icon' => 'fa  fa-cog', 'url' => ['/settings'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Тикеты', 'icon' => 'fa  fa-question', 'url' => ['/ticket'],],
                    ['label' => 'Справочники', 'icon' => 'fa fa-book', 'url' => '#', 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Заказчики', 'url' => ['/client'],],
                        ['label' => 'Должности', 'url' => ['/position'],],
                        ['label' => 'Навыки кандидатов', 'url' => ['/candidate-skill'],],
                        ['label' => 'Тэги кандидатов', 'url' => ['/candidate-tag'],],
                        ['label' => 'Статусы задач', 'url' => ['/task-status'],],
                        ['label' => 'Типы задач', 'url' => ['/task-type'],],
                        ['label' => 'Этапы вакансий', 'url' => ['/project-stage'],],
                        ['label' => 'Типы вакансий', 'url' => ['/project-type'],],
                        ['label' => 'Статусы вакансий', 'url' => ['/project-status'],],
                        ['label' => 'Email шаблоны', 'url' => ['/email-template'],],
                        ['label' => 'Sms шаблоны', 'url' => ['/sms-template'],],
                    ]],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
