<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate_sms`.
 */
class m191015_134804_create_candidate_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate_sms', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->integer()->comment('Кандидат'),
            'text' => $this->text()->comment('Текст'),
            'sent' => $this->boolean()->comment('Отправлено'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-candidate_sms-candidate_id',
            'candidate_sms',
            'candidate_id'
        );

        $this->addForeignKey(
            'fk-candidate_sms-candidate_id',
            'candidate_sms',
            'candidate_id',
            'candidate',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-candidate_sms-candidate_id',
            'candidate_sms'
        );

        $this->dropIndex(
            'idx-candidate_sms-candidate_id',
            'candidate_sms'
        );

        $this->dropTable('candidate_sms');
    }
}
