<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_stage`.
 */
class m191009_173539_create_project_stage_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_stage', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'color' => $this->string()->comment('Цвет'),
            'sorting' => $this->integer()->comment('Сортировка'),
            'as_default' => $this->boolean()->defaultValue(false)->comment('По умолчанию'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project_stage');
    }
}
