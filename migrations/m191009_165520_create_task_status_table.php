<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_status`.
 */
class m191009_165520_create_task_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'color' => $this->string()->comment('Цвет'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task_status');
    }
}
