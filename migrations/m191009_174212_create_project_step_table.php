<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_step`.
 */
class m191009_174212_create_project_step_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_step', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->comment('Проект'),
            'name' => $this->string()->comment('Наименование'),
            'sorting' => $this->integer()->comment('Сортировка'),
            'color' => $this->string()->comment('Цвет'),
        ]);

        $this->createIndex(
            'idx-project_step-project_id',
            'project_step',
            'project_id'
        );

        $this->addForeignKey(
            'fk-project_step-project_id',
            'project_step',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-project_step-project_id',
            'project_step'
        );

        $this->dropIndex(
            'idx-project_step-project_id',
            'project_step'
        );

        $this->dropTable('project_step');
    }
}
