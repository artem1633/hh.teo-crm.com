<?php

use yii\db\Migration;

/**
 * Handles adding phone to table `user`.
 */
class m191020_104150_add_phone_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'phone', $this->string()->after('position_id')->comment('Телефон'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'phone');
    }
}
