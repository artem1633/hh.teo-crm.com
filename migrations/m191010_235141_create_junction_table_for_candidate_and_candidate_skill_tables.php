<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate_candidate_skill`.
 * Has foreign keys to the tables:
 *
 * - `candidate`
 * - `candidate_skill`
 */
class m191010_235141_create_junction_table_for_candidate_and_candidate_skill_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate_candidate_skill', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->integer(),
            'candidate_skill_id' => $this->integer(),
        ]);

        // creates index for column `candidate_id`
        $this->createIndex(
            'idx-candidate_candidate_skill-candidate_id',
            'candidate_candidate_skill',
            'candidate_id'
        );

        // add foreign key for table `candidate`
        $this->addForeignKey(
            'fk-candidate_candidate_skill-candidate_id',
            'candidate_candidate_skill',
            'candidate_id',
            'candidate',
            'id',
            'CASCADE'
        );

        // creates index for column `candidate_skill_id`
        $this->createIndex(
            'idx-candidate_candidate_skill-candidate_skill_id',
            'candidate_candidate_skill',
            'candidate_skill_id'
        );

        // add foreign key for table `candidate_skill`
        $this->addForeignKey(
            'fk-candidate_candidate_skill-candidate_skill_id',
            'candidate_candidate_skill',
            'candidate_skill_id',
            'candidate_skill',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `candidate`
        $this->dropForeignKey(
            'fk-candidate_candidate_skill-candidate_id',
            'candidate_candidate_skill'
        );

        // drops index for column `candidate_id`
        $this->dropIndex(
            'idx-candidate_candidate_skill-candidate_id',
            'candidate_candidate_skill'
        );

        // drops foreign key for table `candidate_skill`
        $this->dropForeignKey(
            'fk-candidate_candidate_skill-candidate_skill_id',
            'candidate_candidate_skill'
        );

        // drops index for column `candidate_skill_id`
        $this->dropIndex(
            'idx-candidate_candidate_skill-candidate_skill_id',
            'candidate_candidate_skill'
        );

        $this->dropTable('candidate_candidate_skill');
    }
}
