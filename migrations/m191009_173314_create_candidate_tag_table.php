<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate_tag`.
 */
class m191009_173314_create_candidate_tag_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate_tag', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('candidate_tag');
    }
}
