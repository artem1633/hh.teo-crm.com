<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_file`.
 */
class m191009_175218_create_project_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_file', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->comment('Проект'),
            'name' => $this->string()->comment('Наименование'),
            'path' => $this->string()->comment('Путь'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-project_file-project_id',
            'project_file',
            'project_id'
        );

        $this->addForeignKey(
            'fk-project_file-project_id',
            'project_file',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-project_file-project_id',
            'project_file'
        );

        $this->dropIndex(
            'idx-project_file-project_id',
            'project_file'
        );

        $this->dropTable('project_file');
    }
}
