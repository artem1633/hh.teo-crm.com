<?php

use yii\db\Migration;

/**
 * Class m191015_134325_add_sms_ru_token_setting
 */
class m191015_134325_add_sms_ru_token_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'token_sms_ru',
            'label' => 'Токен sms.ru'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'token_sms_ru']);
    }
}
