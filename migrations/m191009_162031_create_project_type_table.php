<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_type`.
 */
class m191009_162031_create_project_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'color' => $this->string()->comment('Цвет'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project_type');
    }
}
