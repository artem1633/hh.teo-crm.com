<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate_candidate_tag`.
 * Has foreign keys to the tables:
 *
 * - `candidate`
 * - `candidate_tag`
 */
class m191009_173408_create_junction_table_for_candidate_and_candidate_tag_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate_candidate_tag', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->integer(),
            'candidate_tag_id' => $this->integer(),
        ]);

        // creates index for column `candidate_id`
        $this->createIndex(
            'idx-candidate_candidate_tag-candidate_id',
            'candidate_candidate_tag',
            'candidate_id'
        );

        // add foreign key for table `candidate`
        $this->addForeignKey(
            'fk-candidate_candidate_tag-candidate_id',
            'candidate_candidate_tag',
            'candidate_id',
            'candidate',
            'id',
            'CASCADE'
        );

        // creates index for column `candidate_tag_id`
        $this->createIndex(
            'idx-candidate_candidate_tag-candidate_tag_id',
            'candidate_candidate_tag',
            'candidate_tag_id'
        );

        // add foreign key for table `candidate_tag`
        $this->addForeignKey(
            'fk-candidate_candidate_tag-candidate_tag_id',
            'candidate_candidate_tag',
            'candidate_tag_id',
            'candidate_tag',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `candidate`
        $this->dropForeignKey(
            'fk-candidate_candidate_tag-candidate_id',
            'candidate_candidate_tag'
        );

        // drops index for column `candidate_id`
        $this->dropIndex(
            'idx-candidate_candidate_tag-candidate_id',
            'candidate_candidate_tag'
        );

        // drops foreign key for table `candidate_tag`
        $this->dropForeignKey(
            'fk-candidate_candidate_tag-candidate_tag_id',
            'candidate_candidate_tag'
        );

        // drops index for column `candidate_tag_id`
        $this->dropIndex(
            'idx-candidate_candidate_tag-candidate_tag_id',
            'candidate_candidate_tag'
        );

        $this->dropTable('candidate_candidate_tag');
    }
}
