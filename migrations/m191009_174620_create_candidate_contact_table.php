<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate_contact`.
 */
class m191009_174620_create_candidate_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate_contact', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->integer()->comment('Кандидат'),
            'name' => $this->string()->comment('Наименование'),
            'value' => $this->string()->comment('Значение'),
        ]);

        $this->createIndex(
            'idx-candidate_contact-candidate_id',
            'candidate_contact',
            'candidate_id'
        );

        $this->addForeignKey(
            'fk-candidate_contact-candidate_id',
            'candidate_contact',
            'candidate_id',
            'candidate',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-candidate_contact-candidate_id',
            'candidate_contact'
        );

        $this->dropIndex(
            'idx-candidate_contact-candidate_id',
            'candidate_contact'
        );

        $this->dropTable('candidate_contact');
    }
}
