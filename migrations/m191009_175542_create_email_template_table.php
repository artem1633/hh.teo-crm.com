<?php

use yii\db\Migration;

/**
 * Handles the creation of table `email_template`.
 */
class m191009_175542_create_email_template_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('email_template', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'content' => $this->text()->comment('Содержание'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('email_template');
    }
}
