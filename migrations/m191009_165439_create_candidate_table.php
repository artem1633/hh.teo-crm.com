<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate`.
 */
class m191009_165439_create_candidate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('ФИО'),
            'email' => $this->string()->comment('Email'),
            'phone' => $this->string()->comment('Телефон'),
            'city' => $this->string()->comment('Город'),
            'comment' => $this->text()->comment('Комментарий'),
            'education' => $this->text()->comment('Образование'),
            'work_experience' => $this->text()->comment('Опыт работы'),
            'age' => $this->string()->comment('Возраст'),
            'languages' => $this->text()->comment('Знание языков'),
            'additional' => $this->text()->comment('Дополнительная информация'),
            'doc_file' => $this->string()->comment('Документ'),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('candidate');
    }
}
