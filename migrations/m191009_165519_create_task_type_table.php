<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_type`.
 */
class m191009_165519_create_task_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task_type');
    }
}
