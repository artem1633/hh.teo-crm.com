<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate_file`.
 */
class m191018_154407_create_candidate_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate_file', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->integer()->comment('Кандидат'),
            'name' => $this->string()->comment('Наименование файла'),
            'path' => $this->string()->comment('Путь'),
            'created_at' => $this->dateTime()
        ]);

        $this->createIndex(
            'idx-candidate_file-candidate_id',
            'candidate_file',
            'candidate_id'
        );

        $this->addForeignKey(
            'fk-candidate_file-candidate_id',
            'candidate_file',
            'candidate_id',
            'candidate',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-candidate_file-candidate_id',
            'candidate_file'
        );

        $this->dropIndex(
            'idx-candidate_file-candidate_id',
            'candidate_file'
        );

        $this->dropTable('candidate_file');
    }
}
