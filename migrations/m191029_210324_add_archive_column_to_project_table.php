<?php

use yii\db\Migration;

/**
 * Handles adding archive to table `project`.
 */
class m191029_210324_add_archive_column_to_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('project', 'archive', $this->boolean()->defaultValue(0)->after('status_id')->comment('Архив'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('project', 'archive');
    }
}
