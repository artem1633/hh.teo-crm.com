<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m191009_162556_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->comment('Наименование'),
            'client_id' => $this->integer()->comment('Заказчик'),
            'date_start' => $this->date()->comment('Дата начала'),
            'date_end' => $this->date()->comment('Дата окончания'),
            'type_id' => $this->integer()->comment('Тип'),
            'status_id' => $this->integer()->comment('Статус'),
            'comment' => $this->text()->comment('Комменатрий'),
            'accountable_user_id' => $this->integer()->comment('Ответственный пользователь'),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-project-client_id',
            'project',
            'client_id'
        );

        $this->addForeignKey(
            'fk-project-client_id',
            'project',
            'client_id',
            'client',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-project-type_id',
            'project',
            'type_id'
        );

        $this->addForeignKey(
            'fk-project-type_id',
            'project',
            'type_id',
            'project_type',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-project-status_id',
            'project',
            'status_id'
        );

        $this->addForeignKey(
            'fk-project-status_id',
            'project',
            'status_id',
            'project_status',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-project-accountable_user_id',
            'project',
            'accountable_user_id'
        );

        $this->addForeignKey(
            'fk-project-accountable_user_id',
            'project',
            'accountable_user_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-project-created_by',
            'project',
            'created_by'
        );

        $this->addForeignKey(
            'fk-project-created_by',
            'project',
            'created_by',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-project-created_by',
            'project'
        );

        $this->dropIndex(
            'idx-project-created_by',
            'project'
        );

        $this->dropForeignKey(
            'fk-project-accountable_user_id',
            'project'
        );

        $this->dropIndex(
            'idx-project-accountable_user_id',
            'project'
        );

        $this->dropForeignKey(
            'fk-project-status_id',
            'project'
        );

        $this->dropIndex(
            'idx-project-status_id',
            'project'
        );

        $this->dropForeignKey(
            'fk-project-type_id',
            'project'
        );

        $this->dropIndex(
            'idx-project-type_id',
            'project'
        );

        $this->dropForeignKey(
            'fk-project-client_id',
            'project'
        );

        $this->dropIndex(
            'idx-project-client_id',
            'project'
        );

        $this->dropTable('project');
    }
}
