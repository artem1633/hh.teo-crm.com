<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate_skill`.
 */
class m191010_235050_create_candidate_skill_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate_skill', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('candidate_skill');
    }
}
