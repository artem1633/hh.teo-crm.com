<?php

use yii\db\Migration;

/**
 * Handles adding project_id to table `candidate_sms`.
 */
class m191019_191853_add_project_id_column_to_candidate_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('candidate_sms', 'project_id', $this->integer()->after('candidate_id')->comment('Проект'));

        $this->createIndex(
            'idx-candidate_sms-project_id',
            'candidate_sms',
            'project_id'
        );

        $this->addForeignKey(
            'fk-candidate_sms-project_id',
            'candidate_sms',
            'project_id',
            'project',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-candidate_sms-project_id',
            'candidate_sms'
        );

        $this->dropIndex(
            'idx-candidate_sms-project_id',
            'candidate_sms'
        );

        $this->dropColumn('candidate_sms', 'project_id');
    }
}
