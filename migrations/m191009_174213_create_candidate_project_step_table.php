<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate_project_step`.
 */
class m191009_174213_create_candidate_project_step_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate_project_step', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->integer()->comment('Кандидат'),
            'project_id' => $this->integer()->comment('Проект'),
            'step_id' => $this->integer()->comment('Этап'),
        ]);

        $this->createIndex(
            'idx-candidate_project_step-candidate_id',
            'candidate_project_step',
            'candidate_id'
        );

        $this->addForeignKey(
            'fk-candidate_project_step-candidate_id',
            'candidate_project_step',
            'candidate_id',
            'candidate',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-candidate_project_step-project_id',
            'candidate_project_step',
            'project_id'
        );

        $this->addForeignKey(
            'fk-candidate_project_step-project_id',
            'candidate_project_step',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-candidate_project_step-step_id',
            'candidate_project_step',
            'step_id'
        );

        $this->addForeignKey(
            'fk-candidate_project_step-step_id',
            'candidate_project_step',
            'step_id',
            'project_step',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-candidate_project_step-step_id',
            'candidate_project_step'
        );

        $this->dropIndex(
            'idx-candidate_project_step-step_id',
            'candidate_project_step'
        );

        $this->dropForeignKey(
            'fk-candidate_project_step-project_id',
            'candidate_project_step'
        );

        $this->dropIndex(
            'idx-candidate_project_step-project_id',
            'candidate_project_step'
        );

        $this->dropForeignKey(
            'fk-candidate_project_step-candidate_id',
            'candidate_project_step'
        );

        $this->dropIndex(
            'idx-candidate_project_step-candidate_id',
            'candidate_project_step'
        );

        $this->dropTable('candidate_project_step');
    }
}
