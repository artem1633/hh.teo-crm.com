<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m191009_165521_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->comment('Проект'),
            'executor_id' => $this->integer()->comment('Исполнитель'),
            'candidate_id' => $this->integer()->comment('Кандидат'),
            'executed_at' => $this->dateTime()->comment('Дата и время выполнения'),
            'remember_time' => $this->integer()->comment('Время напоминания (в мин)'),
            'status_id' => $this->integer()->comment('Статус'),
            'type_id' => $this->integer()->comment('Тип'),
            'text' => $this->text()->comment('Текст'),
            'result_text' => $this->text()->comment('Результат'),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-task-project_id',
            'task',
            'project_id'
        );

        $this->addForeignKey(
            'fk-task-project_id',
            'task',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-task-executor_id',
            'task',
            'executor_id'
        );

        $this->addForeignKey(
            'fk-task-executor_id',
            'task',
            'executor_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task-candidate_id',
            'task',
            'candidate_id'
        );

        $this->addForeignKey(
            'fk-task-candidate_id',
            'task',
            'candidate_id',
            'candidate',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task-status_id',
            'task',
            'status_id'
        );

        $this->addForeignKey(
            'fk-task-status_id',
            'task',
            'status_id',
            'task_status',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task-type_id',
            'task',
            'type_id'
        );

        $this->addForeignKey(
            'fk-task-type_id',
            'task',
            'type_id',
            'task_type',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task-created_by',
            'task',
            'created_by'
        );

        $this->addForeignKey(
            'fk-task-created_by',
            'task',
            'created_by',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-task-created_by',
            'task'
        );

        $this->dropIndex(
            'idx-task-created_by',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-type_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-type_id',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-status_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-status_id',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-candidate_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-candidate_id',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-executor_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-executor_id',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-project_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-project_id',
            'task'
        );

        $this->dropTable('task');
    }
}
