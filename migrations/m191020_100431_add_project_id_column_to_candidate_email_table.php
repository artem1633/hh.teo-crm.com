<?php

use yii\db\Migration;

/**
 * Handles adding project_id to table `candidate_email`.
 */
class m191020_100431_add_project_id_column_to_candidate_email_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('candidate_email', 'project_id', $this->integer()->after('candidate_id')->comment('Проект'));

        $this->createIndex(
            'idx-candidate_email-project_id',
            'candidate_email',
            'project_id'
        );

        $this->addForeignKey(
            'fk-candidate_email-project_id',
            'candidate_email',
            'project_id',
            'project',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-candidate_email-project_id',
            'candidate_email'
        );

        $this->dropIndex(
            'idx-candidate_email-project_id',
            'candidate_email'
        );

        $this->dropColumn('candidate_email', 'project_id');
    }
}
