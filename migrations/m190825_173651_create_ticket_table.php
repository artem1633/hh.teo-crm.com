<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ticket`.
 */
class m190825_173651_create_ticket_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ticket', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'subject' => $this->string()->comment('Заголовок'),
            'description' => $this->text()->comment('Описание'),
            'status' => $this->string()->comment('Статус'),
            'is_read' => $this->boolean()->defaultValue(0)->comment('Прочитано'),
            'last_message_datetime' => $this->dateTime()->comment('Дата и время последнего сообщения'),
            'created_at' => $this->dateTime()->comment('Дата и время создания'),
            'closed_datetime' => $this->dateTime()->comment('Дата и время закрытия'),
        ]);

        $this->createIndex(
            'idx-ticket-user_id',
            'ticket',
            'user_id'
        );

        $this->addForeignKey(
            'fk-ticket-user_id',
            'ticket',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-ticket-user_id',
            'ticket'
        );

        $this->dropIndex(
            'idx-ticket-user_id',
            'ticket'
        );

        $this->dropTable('ticket');
    }
}
