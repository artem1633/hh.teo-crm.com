<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170428_140722_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->comment('Логин'),
            'name' => $this->string()->comment('ФИО'),
            'position_id' => $this->integer()->comment('Должность'),
            'role' => $this->integer()->comment('Роль'),
            'avatar' => $this->string()->comment('Аватар'),
            'password_hash' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'access' => $this->boolean()->defaultValue(true)->comment('Доступ'),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно удалить или нельзя'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-user-position_id',
            'user',
            'position_id'
        );

        $this->addForeignKey(
            'fk-user-position_id',
            'user',
            'position_id',
            'position',
            'id',
            'SET NULL'
        );

        $this->insert('user', [
            'login' => 'admin@admin.com',
            'role' => \app\models\User::ROLE_ADMIN,
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'is_deletable' => false,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-user-position_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-position_id',
            'user'
        );


        $this->dropTable('user');
    }
}
