<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_file`.
 */
class m191029_144209_create_task_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_file', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->comment('Задача'),
            'name' => $this->string()->comment('Наименование файла'),
            'path' => $this->string()->comment('Путь'),
            'created_at' => $this->dateTime()
        ]);

        $this->createIndex(
            'idx-task_file-task_id',
            'task_file',
            'task_id'
        );

        $this->addForeignKey(
            'fk-task_file-task_id',
            'task_file',
            'task_id',
            'task',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-task_file-task_id',
            'task_file'
        );

        $this->dropIndex(
            'idx-task_file-task_id',
            'task_file'
        );

        $this->dropTable('task_file');
    }
}
