<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_status`.
 */
class m191009_162142_create_project_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project_status');
    }
}
