<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate_email`.
 */
class m191015_152316_create_candidate_email_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate_email', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->integer()->comment('Кандидат'),
            'text' => $this->text()->comment('Текст'),
            'sent' => $this->boolean()->comment('Отправлено'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-candidate_email-candidate_id',
            'candidate_email',
            'candidate_id'
        );

        $this->addForeignKey(
            'fk-candidate_email-candidate_id',
            'candidate_email',
            'candidate_id',
            'candidate',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-candidate_email-candidate_id',
            'candidate_email'
        );

        $this->dropIndex(
            'idx-candidate_email-candidate_id',
            'candidate_email'
        );
        
        $this->dropTable('candidate_email');
    }
}
