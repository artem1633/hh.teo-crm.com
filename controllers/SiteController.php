<?php

namespace app\controllers;

use app\components\word\Doc;
use app\models\forms\ResetPasswordForm;
use RtfParser\Scanner;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionTest()
    {
        $text = rtf2text('document.rtf');

        $text = str_replace("\n", '', $text);
        $text = str_replace("\r", '', $text);
        $text = str_replace("\t", '', $text);
        $text = str_replace("     ", ' ', $text);
        $text = str_replace("     ", ' ', $text);
        $text = str_replace("     ", ' ', $text);
        $text = str_replace("     ", ' ', $text);
        $text = str_replace("     ", ' ', $text);
        $text = str_replace("   ", ' ', $text);
        $text = str_replace("  ", ' ', $text);
        $text = str_replace("А ", 'А', $text);

        $work = $this->getStringBetween($text, 'Опыт работы', 'Образование');
        $name = iconv_substr($text, 0, mb_strpos($text, ' ', 12, 'utf-8'));
        $age = self::getStringBetween($text, ' , ', 'Проживает');
        $phone = '';
//        $phone = '+'.$this->getStringBetween($text, '+', '— ');
        $education = $this->getStringBetween($text, 'Образование', 'Ключевые навыки');
        if($education == ''){
            $education = $this->getStringBetween($text, 'Образов ание', 'Ключевые навыки');
        }
        $skills = self::getStringBetween($text, 'Навыки', 'История');
        $additional = self::getStringBetween($text, 'Дополнительная информация', 'Мои интересы');
        $email = '';

        echo $text;

        if(  preg_match( '/[+][\d \-\(\)]{5,}/', $text,  $matches ) )
        {
            if(isset($matches[0])){
                $phone = $matches[0];
            }
        }

        if(  preg_match( '/(родил(ась|ся))\s[\d]\s([а-я]+)\s[\d]{4}/ui', $text,  $matches ) )
        {
            if(isset($matches[0])){
                $age = $matches[0];
            }
        }

        if(preg_match('/[^\s]+@[^\s\.]+\.[a-z]+/', $text, $matches))
        {
            if(isset($matches[0])){
                $email = $matches[0];
            }
        }

        if(  preg_match( '/[\d]{2}\s(года|год|лет)/ui', $text,  $matches ) )
        {
            if(isset($matches[0])){
                $age = $matches[0] . ', ' . $age;
            }
        }



        mb_internal_encoding( 'UTF-8');
        mb_regex_encoding( 'UTF-8');

        $skills = mb_split('(?=[А-Я])',$skills);
//            echo '<br><br>';

//            VarDumper::dump($work, 10, true);

        $output = [
            'name' => $name,
            'work' => $work,
            'age' => $age,
            'phone' => $phone,
            'email' => $email,
            'education' => $education,
            'additional' => $additional,
            'skills' => $skills,
        ];

        VarDumper::dump($output, 10, true);

//        $parser = new \SimpleXMLElement($parser);
//
//        $parser->asXML();
//
//        $text = trim(strip_tags($parser->asXML()));
//        $text = str_replace("\n", '', $text);
//        $text = str_replace("\r", '', $text);
//        $text = str_replace("\t", '', $text);
//        $text = str_replace("     ", ' ', $text);
//        $text = str_replace("     ", ' ', $text);
//        $text = str_replace("     ", ' ', $text);
//        $text = str_replace("     ", ' ', $text);
//        $text = str_replace("     ", ' ', $text);
//        $text = str_replace("   ", ' ', $text);
//        $text = str_replace("  ", ' ', $text);
//        $text = str_replace("А ", 'А', $text);
//        VarDumper::dump($text, 10, true);
//
//        $work = self::getStringBetween($text, 'Опыт работы', 'Образование');
//        $name = iconv_substr($text, 0, mb_strpos($text, ' ', 12, 'utf-8'));
//        $age = self::getStringBetween($text, ' , ', 'Проживает');
//        $phone = '+'.self::getStringBetween($text, '+', '— ');
//        $education = self::getStringBetween($text, 'Образование', 'Ключевые навыки');
//        $skills = self::getStringBetween($text, 'Навыки', 'История');
//
//        mb_internal_encoding( 'UTF-8');
//        mb_regex_encoding( 'UTF-8');
//
//        $skills = mb_split('(?=[А-Я])',$skills);
//        $age = iconv_substr($text, (mb_strpos($text, ', ', 0, 'utf-8') + 1), mb_strpos($text, ',', 0, 'utf-8'));
//
//        echo '<br><br>';
//
//        VarDumper::dump($skills, 10, true);
    }

    public function actionTestEmail()
    {
        $result = Yii::$app->mailer->compose()
            ->setFrom('hh.notify@yandex.ru')
            ->setTo('qist27000@gmail.com')
            ->setSubject('Тестирование')
            ->setHtmlBody('<h2>Тестирование отправки почты</h2>')
            ->send();

        var_dump($result);
    }

    private static function getAsXMLContent($xmlElement)
    {
        $content=$xmlElement->asXML();

        $end=strpos($content,'>');
        if ($end!==false)
        {
            $tag=substr($content, 1, $end-1);

            return str_replace(array('<'.$tag.'>', '</'.$tag.'>'), '', $content);
        }
        else
            return '';
    }


    private static function getStringBetween($string, $start, $end){
        $string = ' ' . $string;
        $ini = mb_strpos($string, $start, 0, 'utf-8');
        if ($ini == 0) return '';
        $ini += iconv_strlen($start);
        $len = mb_strpos($string, $end, $ini) - $ini;
        return iconv_substr($string, $ini, $len);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }
}
