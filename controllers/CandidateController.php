<?php

namespace app\controllers;

use app\components\helpers\TagHelper;
use app\models\CandidateEmail;
use app\models\CandidateEmailSearch;
use app\models\CandidateFile;
use app\models\CandidateFileSearch;
use app\models\CandidateProjectStep;
use app\models\CandidateSmsSearch;
use app\models\EmailTemplate;
use app\models\EmailTemplateSearch;
use app\models\forms\DocxForm;
use app\models\forms\EmailForm;
use app\models\forms\SmsForm;
use app\models\ProjectSearch;
use app\models\SmsTemplate;
use Yii;
use app\models\Candidate;
use app\models\CandidateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * CandidateController implements the CRUD actions for Candidate model.
 */
class CandidateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Candidate models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new CandidateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Candidate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $smsSearchModel = new CandidateSmsSearch();
        $smsDataProvider = $smsSearchModel->search([]);
        $smsDataProvider->query->andWhere(['candidate_id' => $id]);

        $emailSearchModel = new CandidateEmailSearch();
        $emailDataProvider = $emailSearchModel->search([]);
        $emailDataProvider->query->andWhere(['candidate_id' => $id]);

        $filesSearchModel = new CandidateFileSearch();
        $filesDataProvider = $filesSearchModel->search([]);
        $filesDataProvider->query->andWhere(['candidate_id' => $id]);

//        $projectSearchModel = new ProjectSearch();
//        $projectDataProvider = $projectSearchModel->search([]);
//        $projectsPks = CandidatePro
//        $projectDataProvider->query->andWhere(['candidate_id' => ]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "кандидат #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                        'smsSearchModel' => $smsSearchModel,
                        'smsDataProvider' => $smsDataProvider,
                        'emailSearchModel' => $emailSearchModel,
                        'emailDataProvider' => $emailDataProvider,
                        'filesSearchModel' => $filesSearchModel,
                        'filesDataProvider' => $filesDataProvider,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'smsSearchModel' => $smsSearchModel,
                'smsDataProvider' => $smsDataProvider,
                'emailSearchModel' => $emailSearchModel,
                'emailDataProvider' => $emailDataProvider,
                'filesSearchModel' => $filesSearchModel,
                'filesDataProvider' => $filesDataProvider,
            ]);
        }
    }

    /**
     * @param integer $candidateId
     * @param string $pjaxContainer
     * @return array
     */
    public function actionSendSms($candidateId, $pjaxContainer = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new SmsForm([
            'candidateId' => $candidateId,
        ]);

        if($model->load($request->post()) && $model->send()){
            return [
                'forceReload' => $pjaxContainer,
                'title' => 'Отправить СМС',
                'content' => '<span class="text-success">СМС отправлено</span>',
                'footer' => Html::button('ОК',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"])
            ];
        } else {
            return [
                'title' => 'Отправить СМС',
                'content' => $this->renderAjax('send-sms', [
                    'model' => $model,
                    'candidateId' => $candidateId,
                ]),
                'footer' => Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]) .
                    Html::button('Отправить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    public function actionGetSmsTemplateText($templateId, $candidateId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $template = SmsTemplate::findOne($templateId);
        $candidate = Candidate::findOne($candidateId);

        if($candidate == null || $template == null){
            return ['error' => 'bad request'];
        }

        $message = TagHelper::handleModel($template->content, [$candidate]);

        return ['message' => $message];
    }

    public function actionDownloadFile($id)
    {
        $candidateFile = CandidateFile::findOne($id);

        if($candidateFile == null){
            throw new NotFoundHttpException();
        }

        if(file_exists($candidateFile->path)){
            Yii::$app->response->sendFile($candidateFile->path, $candidateFile->name);
        }
    }

    public function actionParseDocxFile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new DocxForm();

        if($request->isPost){

            $model->file = UploadedFile::getInstance($model, 'file');

            return $model->parse();
        }

        return ['error' => 1];
    }

    public function actionAddFile($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        return [
            'title'=> "Добавить файлы",
            'content'=>$this->renderAjax('add_file', [
                'model' => $model,
            ]),
            'footer'=> Html::button('Готово',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"])
        ];
    }

    public function actionUploadFile($candidate_id = null)
    {
        $fileName = Yii::$app->security->generateRandomString();
        if(is_dir('uploads') == false){
            mkdir('uploads');
        }
        if(is_dir('uploads/data') == false){
            mkdir('uploads/data');
        }
        $uploadPath = 'uploads/data';

        if (isset($_FILES['file'])) {
            $file = \yii\web\UploadedFile::getInstanceByName('file');
            $path = $uploadPath.'/'.$fileName.'.'.$file->extension;

            if ($file->saveAs($path)) {
                //Now save file data to database
                $candidateFile = new CandidateFile([
                    'candidate_id' => $candidate_id,
                    'name' => $_FILES['file']['name'],
                    'path' => $path
                ]);
                $candidateFile->save(false);

                $file = (array) $file;
                $file['record'] = $candidateFile;

                echo \yii\helpers\Json::encode($file);
            }
        }

        return false;
    }


    /**
     * @param integer $id
     * @return array
     */
    public function actionSendEmail($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new EmailForm([
            'candidateId' => $id,
        ]);

        if($model->load($request->post()) && $model->send()){
            return [
                'title' => 'Отправить Email',
                'content' => '<span class="text-success">сообщение отправлено</span>',
                'footer' => Html::button('ОК',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"])
            ];
        } else {
            return [
                'title' => 'Отправить Email',
                'content' => $this->renderAjax('send-email', [
                    'model' => $model,
                    'candidateId' => $id,
                ]),
                'footer' => Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]) .
                    Html::button('Отправить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    public function actionGetEmailTemplateText($templateId, $candidateId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $template = EmailTemplate::findOne($templateId);
        $candidate = Candidate::findOne($candidateId);

        if($candidate == null || $template == null){
            return ['error' => 'bad request'];
        }

        $message = TagHelper::handleModel($template->content, [$candidate]);

        return ['message' => $message];
    }

    /**
     * Creates a new Candidate model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Candidate();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить кандидата",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить кандидата",
                    'content'=>'<span class="text-success">Создание кандидата успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить кандидата",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Candidate model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить кандидата #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                   'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить кандидата #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Candidate model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionDeleteFromProject($projectId, $candidateId, $pjaxContainer)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $projectCandidate = CandidateProjectStep::find()->where(['project_id' => $projectId, 'candidate_id' => $candidateId])->one();

        if($projectCandidate != null) {
            $projectCandidate->delete();
        }

        return ['forceClose' => true, 'forceReload' => '#'.$pjaxContainer];
    }

     /**
     * Delete multiple existing Candidate model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Candidate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Candidate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Candidate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
