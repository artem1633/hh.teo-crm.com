<?php

namespace app\controllers;

use app\components\helpers\TagHelper;
use app\models\Candidate;
use app\models\CandidateProjectStep;
use app\models\EmailTemplate;
use app\models\forms\EmailForm;
use app\models\forms\SmsForm;
use app\models\ProjectFile;
use app\models\ProjectFileSearch;
use app\models\ProjectStep;
use app\models\ProjectType;
use app\models\SmsTemplate;
use app\models\Task;
use app\models\TaskSearch;
use Yii;
use app\models\Project;
use app\models\ProjectSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $steps = ProjectStep::find()->where(['project_id' => $id])->orderBy('sorting asc')->all();

        $filesSearchModel = new ProjectFileSearch();
        $filesDataProvider = $filesSearchModel->search([]);
        $filesDataProvider->query->andWhere(['project_id' => $id]);

        $tasksSearchModel = new TaskSearch();
        $tasksDataProvider = $tasksSearchModel->search([]);
        $tasksDataProvider->query->andWhere(['project_id' => $id]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Вакансия #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                        'steps' => $steps,
                        'filesSearchModel' => $filesSearchModel,
                        'filesDataProvider' => $filesDataProvider,
                        'tasksSearchModel' => $tasksSearchModel,
                        'tasksDataProvider' => $tasksDataProvider,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $model,
                'steps' => $steps,
                'filesSearchModel' => $filesSearchModel,
                'filesDataProvider' => $filesDataProvider,
                'tasksSearchModel' => $tasksSearchModel,
                'tasksDataProvider' => $tasksDataProvider,
            ]);
        }
    }

    public function actionDownloadFile($id)
    {
        $candidateFile = ProjectFile::findOne($id);

        if($candidateFile == null){
            throw new NotFoundHttpException();
        }

        if(file_exists($candidateFile->path)){
            Yii::$app->response->sendFile($candidateFile->path, $candidateFile->name);
        }
    }

    public function actionUploadFile($project_id = null)
    {
        $fileName = Yii::$app->security->generateRandomString();
        if(is_dir('uploads') == false){
            mkdir('uploads');
        }
        if(is_dir('uploads/data') == false){
            mkdir('uploads/data');
        }
        $uploadPath = 'uploads/data';

        if (isset($_FILES['file'])) {
            $file = \yii\web\UploadedFile::getInstanceByName('file');
            $path = $uploadPath.'/'.$fileName.'.'.$file->extension;

            if ($file->saveAs($path)) {
                //Now save file data to database
                $projectFile = new ProjectFile([
                    'project_id' => $project_id,
                    'name' => $_FILES['file']['name'],
                    'path' => $path
                ]);
                $projectFile->save(false);

                $file = (array) $file;
                $file['record'] = $projectFile;

                echo \yii\helpers\Json::encode($file);
            }
        }

        return false;
    }

    /**
     * Архивирует вакансию
     * @param integer $id
     * @return array
     */
    public function actionArchive($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $model->archive = 1;
        $model->save(false);

        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    /**
     * @param integer $projectId
     * @param integer $candidateId
     * @return array
     */
    public function actionSendSms($projectId, $candidateId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new SmsForm([
            'candidateId' => $candidateId,
            'projectId' => $projectId,
        ]);

        if($model->load($request->post()) && $model->send()){
            return [
                'title' => 'Отправить СМС',
                'content' => '<span class="text-success">СМС отправлено</span>',
                'footer' => Html::button('ОК',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"])
            ];
        } else {
            return [
                'title' => 'Отправить СМС',
                'content' => $this->renderAjax('send-sms', [
                    'model' => $model,
                    'projectId' => $projectId,
                    'candidateId' => $candidateId,
                ]),
                'footer' => Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]) .
                    Html::button('Отправить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    /**
     * @param integer $projectId
     * @param integer $candidateId
     * @return array
     */
    public function actionSendEmail($projectId, $candidateId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new EmailForm([
            'candidateId' => $candidateId,
            'projectId' => $projectId,
        ]);

        if($model->load($request->post()) && $model->send()){
            return [
                'title' => 'Отправить письмо',
                'content' => '<span class="text-success">Письмо отправлено</span>',
                'footer' => Html::button('ОК',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"])
            ];
        } else {
            return [
                'title' => 'Отправить письмо',
                'content' => $this->renderAjax('send-email', [
                    'model' => $model,
                    'projectId' => $projectId,
                    'candidateId' => $candidateId,
                ]),
                'footer' => Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]) .
                    Html::button('Отправить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    public function actionAddFile($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        return [
            'title'=> "Добавить файлы",
            'content'=>$this->renderAjax('add_file', [
                'model' => $model,
            ]),
            'footer'=> Html::button('Готово',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"])
        ];
    }

    /**
     *
     */
    public function actionUpdateStep($projectId, $candidateId, $stepId)
    {
        $step = CandidateProjectStep::find()->where(['project_id' => $projectId, 'candidate_id' => $candidateId])->one();

        if($step == null) {
            throw new NotFoundHttpException();
        }

        if($step->step_id != $stepId){
            $step->step_id = $stepId;
            $step->save(false);
        }
    }

    /**
     * @param $projectId
     * @param $stepId
     */
    public function actionAddCandidate($projectId, $stepId, $pjaxContainer = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $cancleCandidatesPks = ArrayHelper::getColumn(CandidateProjectStep::find()->where(['project_id' => $projectId])->all(), 'candidate_id');

        $candidates = Candidate::find()->where(['not in', 'id', $cancleCandidatesPks])->all();

        $model = new CandidateProjectStep();
        $model->project_id = $projectId;
        $model->step_id = $stepId;

        if($model->load($request->post()) && $model->save()){
            return [
                'forceReload'=>'#'.$pjaxContainer,
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Добавить кандидата",
                'content'=> $this->renderAjax(
                    'add-candidate', [
                        'model' => $model,
                        'candidates' => $candidates
                    ]
                ),
                'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])

            ];
        }

    }

    /**
     * Creates a new Project model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Project();
        $task = new Task();


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить вакансию",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'task' => $task,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->validate()){

                $transaction = Yii::$app->db->beginTransaction();

                if($model->save() == false){
                    return [
                        'title'=> "Добавить вакансию",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                            'task' => $task,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                    ];
                }

                $task->project_id = $model->id;
                if($model->createTask == 1){
                    if($task->load($request->post()) && $task->save()){
                        $transaction->commit();
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'forceClose' => true,
                            'title'=> "Добавить вакансию",
                            'content'=>'<span class="text-success">Создание вакансии успешно завершено</span>',
                            'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                        ];
                    } else {
                        $transaction->rollBack();

                        return [
                            'title'=> "Добавить вакансию",
                            'content'=>$this->renderAjax('create', [
                                'model' => $model,
                                'task' => $task,
                            ]),
                            'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                        ];
                    }
                } else {
                    $transaction->commit();
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'forceClose' => true,
                        'title'=> "Добавить вакансию",
                        'content'=>'<span class="text-success">Создание вакансии успешно завершено</span>',
                        'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                    ];
                }
            }else{           
                return [
                    'title'=> "Добавить вакансию",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'task' => $task,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'task' => $task,
                ]);
            }
        }
    }

    public function actionGetSmsTemplateText($templateId, $projectId, $candidateId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $template = SmsTemplate::findOne($templateId);
        $project = Project::findOne($projectId);
        $candidate = Candidate::findOne($candidateId);

        if($candidate == null || $project == null || $template == null){
            return ['error' => 'bad request'];
        }

        $message = TagHelper::handleModel($template->content, [$project, $candidate]);

        return ['message' => $message];
    }

    public function actionGetEmailTemplateText($templateId, $projectId, $candidateId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $template = EmailTemplate::findOne($templateId);
        $project = Project::findOne($projectId);
        $candidate = Candidate::findOne($candidateId);

        if($candidate == null || $project == null || $template == null){
            return ['error' => 'bad request'];
        }

        $message = TagHelper::handleModel($template->content, [$project, $candidate]);

        return ['message' => $message];
    }

    /**
     * Updates an existing Project model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить вакансию #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить вакансию #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Project model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Project model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
